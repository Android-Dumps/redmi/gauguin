## qssi-user 12
12 SKQ1.211006.001 V14.0.2.0.SJSCNXM release-keys
- Manufacturer: xiaomi
- Platform: lito
- Codename: gauguin
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Kernel Version: 4.19.113
- Id: SKQ1.211006.001
- Incremental: V14.0.2.0.SJSCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/gauguin/gauguin:12/RKQ1.200826.002/V14.0.2.0.SJSCNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211006.001-V14.0.2.0.SJSCNXM-release-keys
- Repo: redmi/gauguin
